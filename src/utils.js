export const categoryIds = {
    index: 0,
    sport: 2,
    fashion: 3,
    tech: 1,
    politics: 4,
    karpov: 6,
};

export const categoryNames = {
    index: 'Главная',
    sport: 'Спорт',
    fashion: 'Мода',
    tech: 'Технологии',
    politics: 'Политика',
    karpov: 'Карпов',
};