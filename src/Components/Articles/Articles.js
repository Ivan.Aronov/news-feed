import React from 'react';
import { MainArticle } from '../MainArticle/MainArticle.js';
import { SmallArticle } from '../SmallArticle/SmallArticle.js';
import './Articles.css';

export const Articles = ( {articles} ) => {
    return (
        <section className="articles">
            <div className="container grid">
                <div className="articles__big-column">
                    {articles.items.slice(0, 3).map((item) => {
                        return (
                            <MainArticle
                                key={item.id}
                                title={item.title}
                                image={item.image}
                                category={articles.categories.find(({id}) => id === item.category_id).name}
                                description={item.description}
                                source={articles.sources.find(({id}) => id === item.source_id).name}
                            />
                        );
                    })}
                </div>
                <div className="articles__small-column">
                    {articles.items.slice(3, 12).map((item) => {
                        const sourceData = articles.sources.find(({id}) => id === item.source_id);

                        return (
                            <SmallArticle
                                key={item.id}
                                title={item.title}
                                date={item.date}
                                source={sourceData.name}
                            />
                        );
                    })};
                </div>
            </div>
        </section>
    )
}