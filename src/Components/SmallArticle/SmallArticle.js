import React from 'react';
import './SmallArticle.css';

export const SmallArticle = ({title, date, source}) => {
    const dateData = new Date(date).toLocaleDateString('ru-RU', {month: 'long', day: 'numeric'});

    return (
        <article className="small-article">
            <h2 className="small-article__title">{title}</h2>
            <span className="small-article__date">{dateData}</span>
            <span className="small-article__caption">{source}</span>
        </article>
    )
}